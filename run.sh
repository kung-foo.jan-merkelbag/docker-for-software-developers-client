#!/bin/bash
if [[ -z ${SERVER_NAME} ]]; then
  echo "Error: env variable SERVER_NAME not set!"
  exit 1
fi

curl \
  -X POST \
  --data '{"username": "Time Bot", "message": "Current time: '"$(date '+%T')"'"}' \
  --silent \
  "${SERVER_NAME}":8000 \
    > /dev/null
